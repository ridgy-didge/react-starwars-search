import React, { Component } from 'react';
import './App.css';
import './Starwars-svg-lines.css';

class StarwarsSkinColour extends Component {
  constructor() {
    super();
    this.state = { }
   
  }

  componentDidMount() { }

  render() {
  
    return (
      <div className="svg-lines-container">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 6000">
          <line class="cls-2" x1="5" y1="0" x2="5" y2="6000"/>
          <line class="cls-1" x1="0" y1="0" x2="0" y2="6000"/>
        </svg>
      </div>
    );
  }}

export default StarwarsSkinColour;