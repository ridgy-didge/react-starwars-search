import React, { Component } from 'react';
import './App.css';
import './Starwars-mass.css';

class StarwarsMass extends Component {
  constructor() {
    super();
    this.state = {
    }
  }

  componentDidMount() {
   
  }

  render() {
    let { people } = this.props;
    let mass = people.slice(0, 1).map( person => (
      person.mass
    ))
    let height = 100;
    let width = height;
    let centerCircle = height / 2;
    let finalmass = mass / 100 * centerCircle;
    return (
      <div className="col-12 col-lg-4 mass-results-container">
        <div className="d-flex justify-content-center">
            <div className="d-flex flex-column mass-results-container justify-content-center">
              <p className="area-title">mass</p>
              <svg className="align-self-center" width={width} height={height} preserveAspectRatio="xMaxYMin meet">
                <circle cx={centerCircle} cy={centerCircle} r={centerCircle - 2} stroke="white" stroke-width="1" fill="" />
                <circle cx={centerCircle} cy={centerCircle} r={finalmass / 2} stroke="" stroke-width="0" fill="white" />
                <text text-anchor="middle" x="71" y="90" class="svg-text">{mass}</text>
              </svg>
            </div>
        </div>
      </div>
    );
  }
}

export default StarwarsMass;