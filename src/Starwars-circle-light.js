import React, { Component } from 'react';
import './App.css';
import './Starwars-circle-light.css';

const colors = ["rgba(255,0,0,1)", "white"];

class StarwarsCircleLight extends Component {
  constructor() {
    super();
    this.state = {
      color: [],
    }
  }

  componentDidMount() {
    this.setState( {
      color: colors[Math.floor(Math.random()*colors.length)]
    })
    
    this.timerID = setInterval(
      () => this.change(),
      Math.floor(Math.random(5000)*10000),
    )
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  change() {
    this.setState( {
      color: colors[Math.floor(Math.random()*colors.length)]
    })
  }

  render() {
    return (
      <div className="circle-light-container">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 900 900" preserveAspectRatio="xMaxYMin meet">
          <circle class="cls-3" cx="450" cy="450" r="444.48" fill={this.state.color}/>
        </svg>
      </div>
    );
  }
}

export default StarwarsCircleLight;