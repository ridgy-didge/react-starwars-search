import React, { Component } from 'react';
import './App.css';
import Starwars from './Starwars-api';
import StarwarsFeed from './Starwars-feed';
import StarwarsSidebarLeft from './Starwars-sidebar-left';
import StarwarsSVGLines from './Starwars-svg-lines';

class App extends Component {
  render() {
    return (
      <div className="App container-fluid d-flex">
        <div className="background-animation-container">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1900 1100" preserveAspectRatio="xMaxYMin meet">

            <g id="grid" transform="translate(-1 8)">
              <line id="Line_7" data-name="Line 7" class="cls-1" y2="1091" transform="translate(1.5 0.5)"/>
              <line id="Line_7-2" data-name="Line 7" class="cls-1" y2="1091" transform="translate(160.5 0.5)"/>
              <line id="Line_7-3" data-name="Line 7" class="cls-1" y2="1091" transform="translate(319.5 0.5)"/>
              <line id="Line_7-4" data-name="Line 7" class="cls-1" y2="1091" transform="translate(478.5 0.5)"/>
              <line id="Line_7-5" data-name="Line 7" class="cls-1" y2="1091" transform="translate(637.5 0.5)"/>
              <line id="Line_7-6" data-name="Line 7" class="cls-1" y2="1091" transform="translate(796.5 0.5)"/>
              <line id="Line_7-7" data-name="Line 7" class="cls-1" y2="1091" transform="translate(955.5 0.5)"/>
              <line id="Line_7-8" data-name="Line 7" class="cls-1" y2="1091" transform="translate(1114.5 0.5)"/>
              <line id="Line_7-9" data-name="Line 7" class="cls-1" y2="1091" transform="translate(1273.5 0.5)"/>
              <line id="Line_7-10" data-name="Line 7" class="cls-1" y2="1091" transform="translate(1432.5 0.5)"/>
              <line id="Line_7-11" data-name="Line 7" class="cls-1" y2="1091" transform="translate(1591.5 0.5)"/>
              <line id="Line_7-12" data-name="Line 7" class="cls-1" y2="1091" transform="translate(1750.5 0.5)"/>
              <line id="Line_8" data-name="Line 8" class="cls-2" x2="1919" transform="translate(1.5 0.5)"/>
              <line id="Line_8-2" data-name="Line 8" class="cls-2" x2="1919" transform="translate(1.5 83.5)"/>
              <line id="Line_8-3" data-name="Line 8" class="cls-2" x2="1919" transform="translate(1.5 166.5)"/>
              <line id="Line_8-4" data-name="Line 8" class="cls-2" x2="1919" transform="translate(1.5 249.5)"/>
              <line id="Line_8-5" data-name="Line 8" class="cls-2" x2="1919" transform="translate(1.5 332.5)"/>
              <line id="Line_8-6" data-name="Line 8" class="cls-2" x2="1919" transform="translate(1.5 415.5)"/>
              <line id="Line_8-7" data-name="Line 8" class="cls-2" x2="1919" transform="translate(1.5 498.5)"/>
              <line id="Line_8-8" data-name="Line 8" class="cls-2" x2="1919" transform="translate(1.5 581.5)"/>
              <line id="Line_8-9" data-name="Line 8" class="cls-2" x2="1919" transform="translate(1.5 664.5)"/>
              <line id="Line_8-10" data-name="Line 8" class="cls-2" x2="1919" transform="translate(1.5 747.5)"/>
              <line id="Line_8-11" data-name="Line 8" class="cls-2" x2="1919" transform="translate(1.5 830.5)"/>
              <line id="Line_8-12" data-name="Line 8" class="cls-2" x2="1919" transform="translate(1.5 913.5)"/>
              <line id="Line_8-13" data-name="Line 8" class="cls-2" x2="1919" transform="translate(1.5 996.5)"/>
              <line id="Line_8-14" data-name="Line 8" class="cls-2" x2="1919" transform="translate(1.5 1079.5)"/>
            </g>
         
            <g id="planets" transform="">
              <g id="moon" data-name="Group 24" class="cls-6">
                <g id="Ellipse_55" data-name="Ellipse 55" class="cls-7" transform="translate(1143 121)">
                  <circle class="cls-7" cx="4" cy="4" r="4"/>
                  <circle class="cls-9" cx="4" cy="4" r="3.5"/>
                </g>
                <g id="Ellipse_56" data-name="Ellipse 56" class="cls-7" transform="translate(1083 170)">
                  <circle class="cls-7" cx="17" cy="17" r="17"/>
                  <circle class="cls-9" cx="17" cy="17" r="16.5"/>
                </g>
                <g id="Ellipse_57" data-name="Ellipse 57" class="cls-7" transform="translate(1171 204)">
                  <circle class="cls-7" cx="14" cy="14" r="14"/>
                  <circle class="cls-9" cx="14" cy="14" r="13.5"/>
                </g>
                <g id="Ellipse_58" data-name="Ellipse 58" class="cls-7" transform="translate(1171 156)">
                  <circle class="cls-7" cx="14" cy="14" r="14"/>
                  <circle class="cls-9" cx="14" cy="14" r="13.5"/>
                </g>
                <g id="Ellipse_54" data-name="Ellipse 54" class="cls-9" transform="translate(1062 87)">
                  <circle class="cls-9" cx="100" cy="100" r="95"/>
                  <circle class="moon-9" cx="100" cy="100" r="99.5"/>
                </g>
              </g>
              <g id="Ellipse_53" data-name="Ellipse 53" class="cls-9" transform="translate(486 188)">
                <circle class="cls-8" cx="394" cy="394" r="394"/>
                <circle class="cls-9" cx="394" cy="394" r="370"/>
              </g>
            </g>
            <g id="spaceship" data-name="Layer 2">
              <polygon class="cls-10" points="1113.54 1096.89 1113.54 1030.09 1232.19 974.42 751.19 236.73 733.36 230.78 729.47 224.05 756.6 211.32 632.27 21 507.95 211.32 533.61 223.36 529.99 229.51 513.43 238 32.36 974.42 146.18 1027.82 146.18 1096.89 288.87 1096.89 288.87 1094.77 561.84 1222.85 561.84 1287.13 704.53 1287.13 704.53 1221.99 971.15 1096.89 1113.54 1096.89"/>
            </g>
          </svg>
        </div>
        <header className="App-header">
        </header>
        <div className="d-none d-md-block col-md-2 relative">
          <StarwarsSidebarLeft />
        </div>
        <div className="col-11 col-md-7 centre-container">
          <Starwars />
        </div>
        <div className="col relative"> 
          <StarwarsSVGLines/>
        </div>
        <div className="d-none d-md-block col-md-3 relative">
          <StarwarsFeed />
        </div>
      </div>
      
    );
  }
}

export default App;
