import React, { Component } from 'react';
import './App.css';
import './Starwars-clock.css';

class StarwarsClock extends Component {
  constructor() {
    super();
    this.state = {
      date: new Date()
    }
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }

  render() {
    return (
      <div className="clock-container">
        <p>
          {this.state.date.toLocaleTimeString()}
        </p>
      </div>
    );
  }
}

export default StarwarsClock;