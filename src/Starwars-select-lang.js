import React, { Component } from 'react';
import './App.css';
import './Starwars-select-lang.css';

class StarwarsLanguageSelect extends Component {
  constructor() {
    super();
    this.state = { 
      language: false,
      elements: []
     };
    this.handleClickHuman = this.handleClickHuman.bind(this);
    this.handleClickAurebash = this.handleClickAurebash.bind(this);
  }

  componentDidMount() {  }

  handleClickHuman(event) {
    event.preventDefault();
    this.setState({language: true});
    this.myRef.current.focus();
  }  

  handleClickAurebash(event) { 
    event.preventDefault(); 
    this.setState({language: false});
  }

  render() {
    const language = this.state.language;

    if (language) {
      console.log(this.state.language);
    } else {
      console.log("goodbye " + this.state.language);
    }

    
    return (
      <div className="select-lang-container align-content-md-center"> 
        <p className="starwars">Language</p>
        <button className="aurebash col" href="" onClick={this.handleClickAurebash}>
          AUREBASH
        </button><br/>
        <button className="starwars col" href="" onClick={this.handleClickHuman}>
          human
        </button>
      </div>
    );
  }
}

export default StarwarsLanguageSelect;