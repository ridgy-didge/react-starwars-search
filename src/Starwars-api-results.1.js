import React, { Component } from 'react';
import './App.css';
import './Starwars-api-results.css';

class StarwarsApiResults extends Component {
  constructor() {
    super();
    this.state = { 
    };
  }

  componentDidMount() {
  }

  render() {
    let circle =  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"  preserveAspectRatio="xMaxYMin meet">
                    <circle className="circ-1" cx="15" cy="15" r="13"/>
                    <circle className="circ-2" cx="15" cy="15" r="9"/>
                    <circle className="circ-3" cx="15" cy="15" r="1"/>
                  </svg>;
  

    return (
      <div className="results-container d-flex justify-content-center">
        {this.props.people.slice(0, 1).map( person => (
          <div className="d-flex flex-column col justify-content-between">
            <div className="d-flex flex-column flex-lg-row">
              <div className="col-12 col-lg-6 d-flex align-items-center">
                <div className="d-none d-sm-flex col-sm-2  col-lg-4 bullet-container">
                  {circle}
                </div>
                <div className="col result-container">
                  <p>name:</p>
                </div>
              </div>
              <div className="col-12 col-sm-8 offset-sm-3 col-lg-6 offset-lg-0 border-bottom d-flex align-items-center justify-content-center">
                <p>{person.name}</p>
              </div>
            </div>
            <div className="d-flex flex-column flex-lg-row">
              <div className="col-12 col-lg-6 d-flex align-items-center">
                <div className="d-none d-sm-flex col-sm-2  col-lg-4 bullet-container">
                  {circle}
                </div>
                <div className="col result-container">
                  <p>height:</p>
                </div>
              </div>
              <div className="col-12 col-sm-8 offset-sm-3 col-lg-6 offset-lg-0 border-bottom d-flex align-items-center justify-content-center">
                <p>{person.height}</p>
              </div>
            </div>
            <div className="d-flex flex-column flex-lg-row">
              <div className="col-12 col-lg-6 d-flex align-items-center">
                <div className="d-none d-sm-flex col-sm-2  col-lg-4 bullet-container">
                  {circle}
                </div>
                <div className="col result-container">
                  <p >mass:</p>
                </div>
              </div>
              <div className="col-12 col-sm-8 offset-sm-3 col-lg-6 offset-lg-0 border-bottom d-flex align-items-center justify-content-center"> 
                <p >{person.mass}</p>
              </div>
            </div>
            <div className="d-flex flex-column flex-lg-row">
              <div className="col-12 col-lg-6 d-flex align-items-center">
                <div className="d-none d-sm-flex col-sm-2  col-lg-4 bullet-container">
                  {circle}
                </div>
                <div className="col result-container">
                  <p>skin colour:</p>
                </div>
              </div>
              <div className="col-12 col-sm-8 offset-sm-3 col-lg-6 offset-lg-0 border-bottom d-flex align-items-center justify-content-center"> 
                <p >{person.skin_color}</p>
              </div>
            </div>
            <div className="d-flex flex-column flex-lg-row">
              <div className="col-12 col-lg-6 d-flex align-items-center">
                <div className="d-none d-sm-flex col-sm-2  col-lg-4 bullet-container">
                  {circle}
                </div>
                <div className="col result-container">
                  <p >eye colour:</p>
                </div>
              </div>
              <div className="col-12 col-sm-8 offset-sm-3 col-lg-6 offset-lg-0 border-bottom d-flex align-items-center justify-content-center"> 
                <p >{person.eye_color}</p>
              </div>
            </div>
            <div className="d-flex flex-column flex-lg-row">
              <div className="col-12 col-lg-6 d-flex align-items-center">
                <div className="d-none d-sm-flex col-sm-2  col-lg-4 bullet-container">
                  {circle}
                </div>
                <div className="col result-container">
                  <p >hair colour:</p>
                </div>
              </div>
              <div className="col-12 col-sm-8 offset-sm-3 col-lg-6 offset-lg-0 border-bottom d-flex align-items-center justify-content-center"> 
                <p >{person.hair_color}</p>
              </div>
            </div>
            <div className="d-flex flex-column flex-lg-row">
              <div className="col-12 col-lg-6 d-flex align-items-center">
                <div className="d-none d-sm-flex col-sm-2  col-lg-4 bullet-container">
                  {circle}
                </div>
                <div className="col result-container">
                  <p >birth year:</p>
                </div>
              </div>
              <div className="col-12 col-sm-8 offset-sm-3 col-lg-6 offset-lg-0 border-bottom d-flex align-items-center justify-content-center"> 
                <p >{person.birth_year}</p>
              </div>
            </div>
            <div className="d-flex flex-column flex-lg-row">
              <div className="col-12 col-lg-6 d-flex align-items-center">
                <div className="d-none d-sm-flex col-sm-2  col-lg-4 bullet-container">
                  {circle}
                </div>
                <div className="col result-container">
                  <p >gender:</p>
                </div>
              </div>
              <div className="col-12 col-sm-8 offset-sm-3 col-lg-6 offset-lg-0 border-bottom d-flex align-items-center justify-content-center"> 
                <p >{person.gender}</p>
              </div>
            </div>
          </div>
        ))} 
      </div>
    );
  }
}

export default StarwarsApiResults;